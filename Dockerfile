# Gatling is a highly capable load testing tool.
#
# Documentation: https://gatling.io/docs/2.3/
# Cheat sheet: http://gatling.io/#/cheat-sheet/2.3
# based on: https://raw.githubusercontent.com/denvazh/gatling/master/2.3.1/Dockerfile

FROM openjdk:8-jdk-alpine

# working directory for gatling
WORKDIR /opt

# gating version
ENV GATLING_VERSION 2.3.1

RUN mkdir -p gatling /run/nginx

RUN apk add --update wget bash nginx && \
  mkdir -p /tmp/downloads && \
  wget -q -O /tmp/downloads/gatling-$GATLING_VERSION.zip \
  https://repo1.maven.org/maven2/io/gatling/highcharts/gatling-charts-highcharts-bundle/$GATLING_VERSION/gatling-charts-highcharts-bundle-$GATLING_VERSION-bundle.zip && \
  mkdir -p /tmp/archive && cd /tmp/archive && \
  unzip /tmp/downloads/gatling-$GATLING_VERSION.zip && \
  mv /tmp/archive/gatling-charts-highcharts-bundle-$GATLING_VERSION/* /opt/gatling/ && \
  rm -rf /tmp/* /opt/gatling/user-files/simulations/* /opt/gatling/user-files/data/*
  

# change context to gatling directory
WORKDIR  /opt/gatling

# set environment variables
ENV PATH /opt/gatling/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV GATLING_HOME /opt/gatling

COPY default.conf /etc/nginx/conf.d/
COPY gatling.sh /opt/gatling/bin/gatling.sh
COPY entrypoint.sh /opt/gatling/bin/entrypoint.sh
RUN chmod +x /opt/gatling/bin/gatling.sh
RUN chmod +x /opt/gatling/bin/entrypoint.sh
EXPOSE 80


ENTRYPOINT ["entrypoint.sh"]
